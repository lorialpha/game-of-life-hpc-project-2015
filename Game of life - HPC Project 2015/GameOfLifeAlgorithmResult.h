#pragma once
class GameOfLifeAlgorithmResult
{
public:
	GameOfLifeAlgorithmResult(GameOfLifeAlgorithmResult* other);
	GameOfLifeAlgorithmResult(bool** result, int w, int h);
	GameOfLifeAlgorithmResult(bool* result, int w, int h);
	~GameOfLifeAlgorithmResult();

	int getWidth();
	int getHeight();
	bool getCellAt(int x, int y);
private:
	bool* storedResult;
	int w;
	int h;

	int getOffset(int x, int y);
};

