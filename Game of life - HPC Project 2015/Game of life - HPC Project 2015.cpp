// Game of life - HPC Project 2015.cpp : definisce il punto di ingresso dell'applicazione.
//

#include "stdafx.h"
#include "Game of life - HPC Project 2015.h"
#include "GameOfLifeAlgorithm.h"
#include "GameOfLifeBaseViewer.h"
#include <iostream>

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")

using namespace std;

#define MAX_LOADSTRING 100
#define ID_EXECUTEALGBTN 12346

// Variabili globali:
HINSTANCE hInst;								// istanza corrente
TCHAR szTitle[MAX_LOADSTRING];					// Testo della barra del titolo
TCHAR szWindowClass[MAX_LOADSTRING];			// nome della classe di finestre principale

// Dichiarazioni con prototipo delle funzioni incluse in questo modulo di codice:
ATOM				MyRegisterClass(HINSTANCE hInstance);
HWND				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

HWND executeAlgorithmButton;
HWND algorithmResultLabel;
HWND mainWindow;

GameOfLifeAlgorithm* myAlgorithm;
GameOfLifeAlgorithmResult* cellsState = NULL;

std::basic_string<TCHAR> labeltext;

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: inserire qui il codice.
	MSG msg;
	HACCEL hAccelTable;

	// Inizializzare le stringhe globali
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_GAMEOFLIFEHPCPROJECT2015, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Eseguire l'inizializzazione dall'applicazione:
	if (!(mainWindow = InitInstance(hInstance, nCmdShow)))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_GAMEOFLIFEHPCPROJECT2015));

	GameOfLifeInitializer initializer(0.7f);
	myAlgorithm = new GameOfLifeAlgorithm(10, 10, initializer);
	
	labeltext += TEXT("Initial alive cells: ");
	TCHAR alivecells[30];
	_itot_s(initializer.getAliveCellsCount(), alivecells, 30, 10);
	labeltext += alivecells;
	labeltext += TEXT("\r\n");

	cout << "Initial alive cells: " << initializer.getAliveCellsCount() << endl;

	// Ciclo di messaggi principale:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	delete myAlgorithm;

	return (int) msg.wParam;
}

//
//  FUNZIONE: MyRegisterClass()
//
//  SCOPO: registra la classe di finestre.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_GAMEOFLIFEHPCPROJECT2015));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_GAMEOFLIFEHPCPROJECT2015);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNZIONE: InitInstance(HINSTANCE, int)
//
//   SCOPO: salva l'handle di istanza e crea la finestra principale
//
//   COMMENTI:
//
//        In questa funzione l'handle di istanza viene salvato in una variabile globale e
//        la finestra di programma principale viene creata e visualizzata.
//
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Memorizzare l'handle di istanza nella variabile globale

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, 600, 600, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return NULL;
	}

	executeAlgorithmButton = CreateWindow( TEXT("button"), TEXT("Esegui algoritmo"), 
		WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_CENTER | WS_EX_WINDOWEDGE,
		5, 5, 150, 30, hWnd, (HMENU)ID_EXECUTEALGBTN, hInstance, NULL);

	algorithmResultLabel = CreateWindow(TEXT("edit"), labeltext.c_str(), 
		WS_VISIBLE | WS_CHILD | ES_MULTILINE , 160, 5, 435, 50, hWnd, (HMENU)3, NULL, NULL);

	ShowWindow(hWnd, nCmdShow);

	UpdateWindow(hWnd);

	return hWnd;
}

//
//  FUNZIONE: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  SCOPO:  elabora i messaggi per la finestra principale.
//
//  WM_COMMAND	- elabora il menu dell'applicazione
//  WM_PAINT	- disegna la finestra principale
//  WM_DESTROY	- inserisce un messaggio di uscita e restituisce un risultato
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Analizzare le selezioni di menu:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_EXECUTEALGBTN:
		{
			TCHAR alivecells[30];
			GameOfLifeAlgorithmSummaryResult* result = myAlgorithm->executeAlgorithm();

			labeltext += TEXT("Cells after iteration: ");
			_itot_s(result->getAliveCells(), alivecells, 30, 10);
			labeltext += alivecells;
			labeltext += TEXT("\r\n");

			cout << "Cells after iteration: " << result->getAliveCells() << endl;

			delete result;

			SetWindowText(algorithmResultLabel, labeltext.c_str());

			if (cellsState) {
				delete cellsState;
			}

			cellsState = myAlgorithm->getCurrentState();
			UpdateWindow(mainWindow);
		}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		if (cellsState && hWnd == mainWindow ) {
			RECT windowRect;
			if (GetWindowRect(hWnd, &windowRect)) {
				GameOfLifeResultViewer* viewer = new GameOfLifeBaseViewer();
				viewer->setResult(cellsState);
				viewer->paint(hdc, 5, 55, 590, 540);
				delete viewer;
			}
		}
		// TODO: aggiungere qui il codice per il disegno...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Gestore dei messaggi della finestra Informazioni su.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
