#pragma once
#include "Algorithm.h"
#include "GameOfLifeAlgorithmSummaryResult.h"
#include "GameOfLifeAlgorithmResult.h"
#include "GameOfLifeInitializer.h"
class GameOfLifeAlgorithm : Algorithm<GameOfLifeAlgorithmSummaryResult>
{
public:
	GameOfLifeAlgorithm(int width, int height, GameOfLifeInitializer& initializer);
	~GameOfLifeAlgorithm();

	bool getCellAt(int x, int y);

	GameOfLifeAlgorithmSummaryResult* executeAlgorithm();
	GameOfLifeAlgorithmResult* getCurrentState();
private:
	void setCellAt(int x, int y, bool value);
	int normalizeY(int y);
	int normalizeX(int x);
	bool* cells;
	int width, height;
};

