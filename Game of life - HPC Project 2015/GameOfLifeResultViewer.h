#pragma once
#include "GameOfLifeAlgorithmResult.h"
class GameOfLifeResultViewer
{
public:
	GameOfLifeResultViewer();
	virtual ~GameOfLifeResultViewer();

	virtual void setResult( GameOfLifeAlgorithmResult* result ) = 0;
	virtual void prepaint(int width, int height) = 0;
	virtual void paint(HDC& graphics, int xFrom, int yFrom, int width, int height) = 0;
};

