#pragma once

class GameOfLifeAlgorithmSummaryResult
{
public:
	GameOfLifeAlgorithmSummaryResult(int aliveCells);
	~GameOfLifeAlgorithmSummaryResult();

	int getAliveCells();
private:
	int aliveCells;
};

