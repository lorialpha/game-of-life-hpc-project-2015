#pragma once
#include "GameOfLifeResultViewer.h"

typedef unsigned char GREYSCALE_PIXEL;

class GameOfLifeBaseViewer :
	public GameOfLifeResultViewer
{
public:
	GameOfLifeBaseViewer();
	~GameOfLifeBaseViewer();
	void setResult(GameOfLifeAlgorithmResult* result);
	void prepaint(int width, int height);
	void paint(HDC& graphics, int xFrom, int yFrom, int width, int height);
private:
	GameOfLifeAlgorithmResult* state;

	GREYSCALE_PIXEL blend(float fieldX, float fieldY, float width, float height);
};

