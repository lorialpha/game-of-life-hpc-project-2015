#pragma once
template <class T>
class Algorithm
{
public:
	virtual ~Algorithm() {};

	virtual T* executeAlgorithm() = 0;
};

