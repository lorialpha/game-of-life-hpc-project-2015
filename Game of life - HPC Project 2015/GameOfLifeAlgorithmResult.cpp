#include "stdafx.h"
#include "GameOfLifeAlgorithmResult.h"
#include <iostream>

GameOfLifeAlgorithmResult::GameOfLifeAlgorithmResult(GameOfLifeAlgorithmResult* other)
{
	if ( other == NULL ) {
		throw "Null copy parameter";
	}

	this->w = other->getWidth();
	this->h = other->getHeight();

	if (w <= 0 || h <= 0) {
		throw "Invalid width or height";
	}

	this->storedResult = new bool[w*h];

	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			storedResult[getOffset(x, y)] = other->getCellAt(x,y);
		}
	}
}

GameOfLifeAlgorithmResult::GameOfLifeAlgorithmResult(bool** result, int w, int h)
{
	if (result == NULL) {
		throw "Null copy parameter";
	}

	this->w = w;
	this->h = h;

	if (w <= 0 || h <= 0) {
		throw "Invalid width or height";
	}

	this->storedResult = new bool[w*h];

	for (int y = 0; y < h; y++) {
		if (result[y] == NULL) {
			throw "Null copy parameter (subscript)";
		}

		for (int x = 0; x < w; x++) {
			
			storedResult[getOffset(x, y)] = result[y][x];
		}
	}
}

GameOfLifeAlgorithmResult::GameOfLifeAlgorithmResult(bool* result, int w, int h)
{
	if (result == NULL) {
		throw "Null copy parameter";
	}

	this->w = w;
	this->h = h;

	if (w <= 0 || h <= 0) {
		throw "Invalid width or height";
	}

	this->storedResult = new bool[w*h];

	for (int y = 0; y < h; y++) {
		for (int x = 0; x < w; x++) {
			int offset = getOffset(x, y);
			storedResult[offset] = result[offset];
		}
	}
}

GameOfLifeAlgorithmResult::~GameOfLifeAlgorithmResult()
{
	delete this->storedResult;
}

int GameOfLifeAlgorithmResult::getWidth() 
{
	return w;
}


int GameOfLifeAlgorithmResult::getHeight()
{
	return h;
}

bool GameOfLifeAlgorithmResult::getCellAt(int x, int y)
{
	std::cout << "X: " << x << "Y: " << y << std::endl;
	if (x >= w || y >= h) {
		return false;
		//throw "Out of bounds";
	}

	return storedResult[getOffset(x, y)];
}

int GameOfLifeAlgorithmResult::getOffset(int x, int y)
{
	return y*w + x;
}
