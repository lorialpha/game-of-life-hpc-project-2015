#include "stdafx.h"
#include "GameOfLifeAlgorithmSummaryResult.h"


GameOfLifeAlgorithmSummaryResult::GameOfLifeAlgorithmSummaryResult(int aliveCells)
{
	this->aliveCells = aliveCells;
}


GameOfLifeAlgorithmSummaryResult::~GameOfLifeAlgorithmSummaryResult()
{
}

int GameOfLifeAlgorithmSummaryResult::getAliveCells() {
	return this->aliveCells;
}
