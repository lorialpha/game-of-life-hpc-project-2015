#include "stdafx.h"
#include "GameOfLifeAlgorithm.h"


GameOfLifeAlgorithm::GameOfLifeAlgorithm(int width, int height, GameOfLifeInitializer& initializer)
{
	if (width <= 0 || height <= 0) {
		throw "Invalid width or height";
	}

	this->width = width;
	this->height = height;

	this->cells = new bool[width * height];

	for (int i = 0; i < width*height; i++)  {
		this->cells[i] = initializer.getNextCell();
	}
}

GameOfLifeAlgorithm::~GameOfLifeAlgorithm()
{
	delete this->cells;
}

bool GameOfLifeAlgorithm::getCellAt(int x, int y)
{
	return this->cells[y*width + x];
}

GameOfLifeAlgorithmSummaryResult* GameOfLifeAlgorithm::executeAlgorithm() {
	int aliveCells = 0;
	for (int y = 1; y < height-1; y++) {
		for (int x = 1; x < width-1; x++) {
			int realX = normalizeX(x);
			int realY = normalizeY(y);

			int nearby = 0;
			nearby += getCellAt(realX - 1, realY - 1);
			nearby += getCellAt(realX, realY - 1);
			nearby += getCellAt(realX + 1, realY - 1);

			nearby += getCellAt(realX - 1, realY);
			nearby += getCellAt(realX + 1, realY);

			nearby += getCellAt(realX - 1, realY + 1);
			nearby += getCellAt(realX, realY + 1);
			nearby += getCellAt(realX + 1, realY + 1);


			if (getCellAt(realX, realY)) {
				if (nearby < 2 || nearby > 3) {
					setCellAt(realX, realY, false);
				}
				else {
					aliveCells++;
				}
			} else {
				if ( nearby == 3) {
					setCellAt(realX, realY, true);
					aliveCells++;
				}
			}
		}
	}

	return new GameOfLifeAlgorithmSummaryResult( aliveCells );
}

GameOfLifeAlgorithmResult* GameOfLifeAlgorithm::getCurrentState() {
	return new GameOfLifeAlgorithmResult(cells, width, height);
}

int GameOfLifeAlgorithm::normalizeY(int y) {
	if (y >= height) {
		return y % height;
	}
	else {
		return y;
	}
}

int GameOfLifeAlgorithm::normalizeX(int x) {
	if (x >= width) {
		return x % width;
	}
	else {
		return x;
	}
}

void GameOfLifeAlgorithm::setCellAt(int x, int y, bool value) {
	this->cells[y*width + x] = value;
}
