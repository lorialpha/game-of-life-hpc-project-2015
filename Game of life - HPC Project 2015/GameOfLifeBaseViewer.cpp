#include "stdafx.h"
#include "GameOfLifeBaseViewer.h"
#include <cmath>

#define PIXEL_CUT 0.4f

GameOfLifeBaseViewer::GameOfLifeBaseViewer() : state(NULL)
{
}


GameOfLifeBaseViewer::~GameOfLifeBaseViewer()
{
	this->state = NULL;
}

void GameOfLifeBaseViewer::setResult(GameOfLifeAlgorithmResult* result)
{
	this->state = result;
}

void GameOfLifeBaseViewer::prepaint(int width, int height)
{
	if (state == NULL) {
		throw "Algorithm result not set";
	}
}

void GameOfLifeBaseViewer::paint(HDC& graphics, int xFrom, int yFrom, int width, int height)
{
	if (state == NULL) {
		throw "Algorithm result not set";
	}

	float elementsPerPixelX = (float)state->getWidth() / (float)width;
	float elementsPerPixelY = (float)state->getHeight() / (float)height;

	bool zoomedX = elementsPerPixelX <= 1.0f;
	bool zoomedY = elementsPerPixelY <= 1.0f;

	if ((!zoomedX) && (!zoomedY)) {
		//Troppi elementi sia sull'asse X che Y
		int realX = xFrom;
		int realY = yFrom;
		for (float y = 0.0f; y < state->getHeight(); y += elementsPerPixelY) {
			realX = xFrom;
			for (float x = 0.0f; x < state->getWidth(); x += elementsPerPixelX) {
				GREYSCALE_PIXEL currentPixel = 255 - blend(x, y, elementsPerPixelX, elementsPerPixelY);

				SetPixel(graphics, realX, realY, RGB(currentPixel, currentPixel, currentPixel));
				realX++;
			}
			realY++;
		}
	}
	else if (zoomedX && !zoomedY) {
		//Pochi elementi sull'asse X, troppi sull'asse Y
		float pixelsPerElement = (float)width / (float)state->getWidth();

		int realX = xFrom;
		int realY = yFrom;
		for (float y = 0.0f; y < state->getHeight(); y += elementsPerPixelY) {
			realX = xFrom;
			float x = 0.0f;
			for (; x < state->getWidth(); x += elementsPerPixelX) {
				int currentPixelX = (int)x;
				GREYSCALE_PIXEL currentPixel = 255 - blend(x, y, elementsPerPixelX, elementsPerPixelY);
				for (; x < state->getWidth(), currentPixelX == (int)x; x += elementsPerPixelX) {
					SetPixel(graphics, realX, realY, RGB(currentPixel, currentPixel, currentPixel));
					realX++;
				}
			}
			realY++;
		}
	}
	else if (zoomedY && !zoomedX) {
		//Pochi elementi sull'asse Y, troppi sull'asse X

		//TODO: non ottimizzato
		int realX = xFrom;
		int realY = yFrom;
		for (float y = 0.0f; y < state->getHeight(); y += elementsPerPixelY) {
			realX = xFrom;
			for (float x = 0.0f; x < state->getWidth(); x += elementsPerPixelX) {
				GREYSCALE_PIXEL currentPixel = 255 - blend(x, y, elementsPerPixelX, elementsPerPixelY);

				SetPixel(graphics, realX, realY, RGB(currentPixel, currentPixel, currentPixel));
				realX++;
			}
			realY++;
		}
	}
	else {
		//Pochi elementi sull'asse Y, pochi sull'asse X

		//TODO: non ottimizzato
		float pixelsPerElement = (float)width / (float)state->getWidth();

		int realX = xFrom;
		int realY = yFrom;
		for (float y = 0.0f; y < state->getHeight(); y += elementsPerPixelY) {
			realX = xFrom;
			float x = 0.0f;
			for (; x < state->getWidth(); x += elementsPerPixelX) {
				int currentPixelX = (int)x;
				GREYSCALE_PIXEL currentPixel = 255-blend(x, y, elementsPerPixelX, elementsPerPixelY);
				for (; x < state->getWidth(), currentPixelX == (int)x; x += elementsPerPixelX) {
					SetPixel(graphics, realX, realY, RGB(currentPixel, currentPixel, currentPixel));
					realX++;
				}
			}
			realY++;
		}
		blend(0, 0, 0, 0);
	}
	
}

GREYSCALE_PIXEL GameOfLifeBaseViewer::blend(float fieldX, float fieldY, float width, float height)
{
	int fromX, fromY, toX, toY;
	float nPixels;
	int alivePixels = 0;
	float partialAlivePixels = 0.0f;
	GREYSCALE_PIXEL result = 0;

	int fieldWidth = state->getWidth();
	int fieldHeight = state->getHeight();

	{
		//TRIM width AND height TO FIELD LIMITS
		if ((fieldX + width) > fieldWidth) {
			width = fieldWidth - fieldX;
		}

		if ((fieldY + height) > fieldHeight) {
			height = fieldHeight - fieldY;
		}

		if (width <= 0 || height <= 0) {
			return result;
		}
	}
	
	{
		//CALCULATE INTEGER ITERATION BOUNDS AND NUMBER OF PIXELS (USED TO WEIGHT NUMBERS)
		fromX = (int) ceil(fieldX);
		fromY = (int) ceil(fieldY);
		toX = (int) min(floor(fieldX + width), fieldWidth);
		toY = (int) min(floor(fieldY + height), fieldHeight);

		nPixels = width*height;
	}

	{
		//INTEGER ITERATION
		if (fromY < toY && fromX < toX) {

			for (int y = fromY; y < toY; y++) {
				for (int x = fromX; x < toX; x++) {
					alivePixels += state->getCellAt(x, y);
				}
			}
		}
		else if (fromY < toY) {
			for (int y = fromY; y < toY; y++) {
				alivePixels += state->getCellAt(fromX, y);
			}
		}
		else if (fromX < toX) {
			for (int x = fromX; x < toX; x++) {
				alivePixels += state->getCellAt(x, fromY);
			}
		}
		else {
			alivePixels += state->getCellAt(fromX, fromY);
		}
	}

	{
		//FLOAT ITERATION
		if (fromX <= toX) {
			float lowerBoundX = floor(fieldX);
			if ((fieldX - lowerBoundX) < PIXEL_CUT) {
				float coeff = 1.0f - (fieldX - lowerBoundX);
				for (int y = fromY; y < toY; y++) {
					partialAlivePixels += coeff*state->getCellAt((int)lowerBoundX, y);
				}
			}

			float upperBoundX = ceil(fieldX + width);
			if (upperBoundX - (fieldX + width) < PIXEL_CUT) {
				float coeff = 1.0f - (upperBoundX - (fieldX + width));
				for (int y = fromY; y < toY; y++) {
					partialAlivePixels += coeff*state->getCellAt((int)upperBoundX, y);
				}
			}

		}

		if (fromY <= toY) {
			float lowerBoundY = floor(fieldY);
			if ((fieldY - lowerBoundY) < PIXEL_CUT) {
				float coeff = 1.0f - (fieldY - lowerBoundY);
				for (int x = fromX; x < toX; x++) {
					partialAlivePixels += coeff*state->getCellAt(x, (int)lowerBoundY);
				}
			}

			float upperBoundY = ceil(fieldY + height);
			if (upperBoundY - (fieldY + height) < PIXEL_CUT) {
				float coeff = 1.0f - (upperBoundY - (fieldY + height));
				for (int x = fromX; x < toX; x++) {
					partialAlivePixels += coeff*state->getCellAt(x, (int)upperBoundY);
				}
			}
		}

		//TODO: corners?
	}

	partialAlivePixels += alivePixels;

	result = (int) (partialAlivePixels / nPixels);

	return result;
}
