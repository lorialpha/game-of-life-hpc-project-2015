#pragma once
#include <random>

class GameOfLifeInitializer
{
public:
	GameOfLifeInitializer(float probability);
	virtual ~GameOfLifeInitializer();

	virtual bool getNextCell();
	
	virtual int getAliveCellsCount();
private:
	std::random_device rd;
	std::mt19937 gen;
	std::uniform_real_distribution<> dis;
	float probability;
	int count;
};

