#pragma once
template <class T>
class AlgorithmResultComparator
{
public:
	virtual ~AlgorithmResultComparator() = 0;

	virtual bool compareForEquality(T* resultA, T* resultB) = 0;
};

