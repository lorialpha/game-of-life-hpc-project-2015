#include "stdafx.h"
#include "GameOfLifeInitializer.h"

GameOfLifeInitializer::GameOfLifeInitializer(float probability) : gen(rd()), dis(0.0, 1.0), count(0)
{
	this->probability = probability;
}


GameOfLifeInitializer::~GameOfLifeInitializer()
{
}

bool GameOfLifeInitializer::getNextCell()
{
	if (dis(gen) < probability) {
		count++;
		return true;
	}

	return false;
}

int GameOfLifeInitializer::getAliveCellsCount() {
	return count;
}
