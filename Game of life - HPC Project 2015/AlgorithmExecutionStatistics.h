#pragma once
#include <string>

class AlgorithmExecutionStatistics
{
public:
	virtual ~AlgorithmExecutionStatistics() = 0;

	virtual long getExecutionTimeMillis() = 0;

	virtual bool hasCorrectResult() = 0;

	virtual std::wstring getAlgorithmName() = 0;

	/* ... others to be added */
};

